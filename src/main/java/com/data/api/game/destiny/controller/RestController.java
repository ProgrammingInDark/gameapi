package com.data.api.game.destiny.controller;

import com.data.api.game.destiny.service.milestoneService;
import com.data.api.game.destiny.utility.milestonepojoinfo.MilestoneInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;

@RequestMapping("api/destiny")
@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Autowired
    private milestoneService milestoneService;

    @GetMapping("/milestones")
    public List<MilestoneInfo> getMilestoneInformation() throws IOException {

        return milestoneService.getMilestoneInfo();
    }
}
