package com.data.api.game.destiny.dao;

import com.data.api.game.destiny.model.DestinyObjectiveDefinition;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObjectiveRepository extends CrudRepository<DestinyObjectiveDefinition, Long> {

}
