package com.data.api.game.destiny.service;

import com.data.api.game.destiny.dao.ActivityModifierRepository;
import com.data.api.game.destiny.dao.ActivityRepository;
import com.data.api.game.destiny.dao.MilestoneRepository;
import com.data.api.game.destiny.dao.ObjectiveRepository;
import com.data.api.game.destiny.endpoints.DestinyPublicEndpoints;
import com.data.api.game.destiny.pojo.Milestones;
import com.data.api.game.destiny.utility.MilestoneUtility;
import com.data.api.game.destiny.utility.milestonepojoinfo.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@CacheConfig(cacheNames = "milestones")
public class milestoneService {

    @Autowired
    private DestinyPublicEndpoints destinyPublicEndpoints;

    @Autowired
    private MilestoneRepository milestoneRepository;

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private ObjectiveRepository objectiveRepository;

    @Autowired
    private ActivityModifierRepository activityModifierRepository;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MilestoneUtility milestoneUtility;

    @Cacheable
    public List<MilestoneInfo> getMilestoneInfo() throws IOException {

        Milestones milestones = destinyPublicEndpoints.getMilestones();

        List<MilestoneInfo> milestoneInfos = new ArrayList<MilestoneInfo>();
        List<MilestoneUtility.MilestoneId> milestoneIds = new ArrayList<MilestoneUtility.MilestoneId>();
        milestoneIds.addAll(milestoneUtility.getMilestoneIds(milestones.getMap()));

        for (MilestoneUtility.MilestoneId milestoneId : milestoneIds) {

            Long milestoneIdd = milestoneId.getMilestoneId();

            JsonNode milestoneNode = mapper.readTree(milestoneRepository.findById(milestoneIdd).get().getJson());

            String milestoneName = milestoneNode.get("displayProperties").get("name").asText();
            String milestoneIcon = "";

            if (milestoneNode.get("displayProperties").get("hasIcon").asBoolean() == true) {

                milestoneIcon = milestoneNode.get("displayProperties").get("icon").asText();
            } else {
                milestoneIcon = null;
            }

            milestoneInfos.add(new MilestoneInfo(milestoneName, milestoneIcon,
                    getActivityInfo(milestoneId.getActivitiesIds()), getQuestInfo(milestoneId.getQuestHashes(), milestoneIdd),
                    milestoneId.getStartDate(), milestoneId.getEndDate()));
        }

        return milestoneInfos;
    }

    public List<ActivityInfo> getActivityInfo(List<MilestoneUtility.ActivityId> activitiesIds) throws IOException {

        List<ActivityInfo> activityInfos = new ArrayList<ActivityInfo>();

        if (activitiesIds == null) {
            return null;
        } else {

            for (MilestoneUtility.ActivityId activityId : activitiesIds) {

                Long activityIdd = activityId.getActivityId();

                JsonNode activityNode = mapper.readTree(activityRepository.findById(activityIdd).get().getJson());

                String activityName = activityNode.get("displayProperties").get("name").asText();
                String activityIcon = "";

                if (activityNode.get("displayProperties").get("hasIcon").asBoolean() == true) {

                    activityIcon = activityNode.get("displayProperties").get("icon").asText();
                } else {
                    activityIcon = null;
                }

                activityInfos.add(new ActivityInfo(activityName, activityIcon,
                        getObjectiveInfo(activityId.getObjectiveIds()), getModifierInfo(activityId.getModifierIds())));
            }
        }
        return activityInfos;
    }

    public List<QuestInfo> getQuestInfo(List<MilestoneUtility.AvailableQuestHash> questHashes, Long milestoneId) throws IOException {

        List<QuestInfo> questInfos = new ArrayList<QuestInfo>();

        if (questHashes == null) {
            return null;
        } else {

            for (MilestoneUtility.AvailableQuestHash questHash : questHashes) {

                Long questHashe = questHash.getQuestHash();

                JsonNode questNode = mapper.readTree(milestoneRepository.findById(milestoneId).get().getJson());
                String questDescription = questNode.get("quests").get(String.valueOf(questHashe))
                        .get("displayProperties").get("description").asText();
                String questName = questNode.get("quests").get(String.valueOf(questHashe))
                        .get("displayProperties").get("name").asText();
                String questIcon = "";

                if (questNode.get("quests").get(String.valueOf(questHashe)).get("displayProperties").get("hasIcon")
                        .asBoolean() == true) {

                    questIcon = questNode.get("quests").get(String.valueOf(questHashe)).get("displayProperties")
                            .get("icon").asText();
                } else {
                    questIcon = null;
                }

                questInfos.add(new QuestInfo(questDescription, questName, questIcon));
            }
        }
        return questInfos;
    }

    public List<ObjectiveInfo> getObjectiveInfo(List<Long> objectivesIds) throws IOException {

        List<ObjectiveInfo> objectiveInfos = new ArrayList<ObjectiveInfo>();

        if (objectivesIds == null) {
            return null;
        } else {

            for (Long objectiveId : objectivesIds) {

                JsonNode objectiveNode = mapper.readTree(objectiveRepository.findById(objectiveId).get().getJson());
                String objectiveDescription = objectiveNode.get("displayProperties").get("description").asText();
                int completionValue = objectiveNode.get("completionValue").asInt();

                objectiveInfos.add(new ObjectiveInfo(objectiveDescription, completionValue));
            }
        }
        return objectiveInfos;
    }

    public List<ModifierInfo> getModifierInfo(List<Long> modifierIds) throws IOException {

        List<ModifierInfo> modifierInfos = new ArrayList<ModifierInfo>();

        if (modifierIds == null) {
            return null;
        } else {

            for (Long modifierId : modifierIds) {

                JsonNode modifierNode = mapper.readTree(activityModifierRepository.findById(modifierId).get().getJson());

                String modifierDescription = modifierNode.get("displayProperties").get("description").asText();
                String modifierName = modifierNode.get("displayProperties").get("name").asText();
                String modifierIcon = "";

                if (modifierNode.get("displayProperties").get("hasIcon").asBoolean() == true) {
                    modifierIcon = modifierNode.get("displayProperties").get("icon").asText();
                } else {
                    modifierIcon = null;
                }

                modifierInfos.add(new ModifierInfo(modifierDescription, modifierName, modifierIcon));
            }
        }
        return modifierInfos;
    }
}
