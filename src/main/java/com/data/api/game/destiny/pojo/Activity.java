package com.data.api.game.destiny.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
public class Activity {

    @JsonProperty("activityHash")
    private long activityHash;

    @JsonProperty("challengeObjectiveHashes")
    private List<Long> challengeObjectiveHashes;

    @JsonProperty("modifierHashes")
    private List<Long> modifierHashes;

    @JsonProperty("phaseHashes")
    private List<Long> phaseHashes;
}
