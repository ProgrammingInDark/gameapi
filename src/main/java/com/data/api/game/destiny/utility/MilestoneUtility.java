package com.data.api.game.destiny.utility;

import com.data.api.game.destiny.pojo.Activity;
import com.data.api.game.destiny.pojo.AvailableQuests;
import com.data.api.game.destiny.pojo.Milestone;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class MilestoneUtility {

    public long hashToId(long hash) {
        long val = hash;

        if ((val & (1L << (32L - 1L))) != 0) {
            val = val - (1L << 32L);
        }

        return val;
    }

    public List<MilestoneId> getMilestoneIds(HashMap<String, Milestone> milestoneMap) {
        List<MilestoneId> milestoneIds = new ArrayList<MilestoneId>();

        for (Milestone milestoneSet : milestoneMap.values()) {

            long milestoneId = hashToId(milestoneSet.getMilestoneHash());

            List<ActivityId> activityIds = new ArrayList<ActivityId>();

            List<AvailableQuestHash> questHashes = new ArrayList<AvailableQuestHash>();

            if (milestoneSet.getActivities() != null) {
                activityIds.addAll(this.getActivityIds(milestoneSet.getActivities()));
            } else {
                activityIds = null;
            }

            if (milestoneSet.getAvailableQuests() != null) {
                questHashes.addAll(this.getQuestHashes(milestoneSet.getAvailableQuests()));
            } else {
                questHashes = null;
            }

            String startDate = milestoneSet.getStartDate();
            String endDate = milestoneSet.getEndDate();

            milestoneIds.add(new MilestoneId(milestoneId, activityIds, questHashes, startDate, endDate));
        }

        return milestoneIds;
    }

    public List<ActivityId> getActivityIds(List<Activity> activities) {

        List<ActivityId> activityIds = new ArrayList<ActivityId>();

        for (Activity activity : activities) {
            long activityId = hashToId(activity.getActivityHash());

            List<Long> objectiveIds = new ArrayList<Long>();
            List<Long> modifierIds = new ArrayList<Long>();

            if (activity.getChallengeObjectiveHashes() != null) {
                objectiveIds.addAll(this.getObjectiveIds(activity.getChallengeObjectiveHashes()));
            } else {
                objectiveIds = null;
            }

            if (activity.getModifierHashes() != null) {
                modifierIds.addAll(this.getModifierIds(activity.getModifierHashes()));
            } else {
                modifierIds = null;
            }

            activityIds.add(new ActivityId(activityId, objectiveIds, modifierIds));
        }
        return activityIds;
    }

    public List<AvailableQuestHash> getQuestHashes(List<AvailableQuests> quests) {

        List<AvailableQuestHash> questHashes = new ArrayList<AvailableQuestHash>();

        for (AvailableQuests quest : quests) {
            long questHash = quest.getQuestItemHash();
            questHashes.add(new AvailableQuestHash(questHash));
        }
        return questHashes;
    }

    public List<Long> getObjectiveIds(List<Long> objectiveHashes) {

        List<Long> objectiveIds = new ArrayList<Long>();

        for (long objectiveHash : objectiveHashes) {
            long objectiveId = hashToId(objectiveHash);
            objectiveIds.add(objectiveId);
        }

        return objectiveIds;
    }

    public List<Long> getModifierIds(List<Long> modifierHashes) {

        List<Long> modifierIds = new ArrayList<Long>();

        for (long modifierHash : modifierHashes) {
            long modifierId = hashToId(modifierHash);
            modifierIds.add(modifierId);
        }

        return modifierIds;
    }

    @Data
    @AllArgsConstructor
    public class MilestoneId {
        private long milestoneId;
        private List<ActivityId> activitiesIds;
        private List<AvailableQuestHash> questHashes;
        private String startDate;
        private String endDate;
    }

    @Data
    @AllArgsConstructor
    public class ActivityId {
        private long activityId;
        private List<Long> objectiveIds;
        private List<Long> modifierIds;

    }

    @Data
    @AllArgsConstructor
    public class AvailableQuestHash {
        private long questHash;

    }
}
