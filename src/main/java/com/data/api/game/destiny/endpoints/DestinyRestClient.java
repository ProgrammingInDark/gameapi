package com.data.api.game.destiny.endpoints;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Component
public class DestinyRestClient {

    @Bean
    @Qualifier("destiny")
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }

    @Bean
    @Qualifier("destiny")
    public HttpEntity<String> httpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("User-Agent", "Spring Destiny App/0.1 (bassawdavid@gmail.com)");
        headers.add("X-API-KEY", "a306786042c54930afde18bfc46daa90");
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return entity;
    }
}
